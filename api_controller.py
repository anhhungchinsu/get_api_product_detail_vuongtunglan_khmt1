
from odoo import http, _
from odoo.http import request
from odoo.addons.lc_rest_api.controllers.controllers import validate_token, generate_response, getRequestIP, serialization_data, toUserDatetime, _args, permission_check
from odoo.addons.http_routing.models.ir_http import slug

#api get product detail
class ApiGetData(http.Controller):
    @validate_token
    @http.route(['/api/get/product/', '/api/get/product/<product_id>'], methods=['GET'], type='http', auth='none', csrf=False)
    def model_pos_config(self, access_token, product_id=None, **kw):
        if not permission_check('product.template', 'read'):
            return generate_response(data={
                'success': False,
                'msg': "Create permission denied"
            })
        if not product_id:
            try:
                _domain, _fields, _offset, _limit, _order = _args(kw)
                data = request.env['product.template'].search_read(fields=['id', 'name'], offset=_offset, limit=_limit, order=_order)
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data),
                    'data_count': len(data)
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
        else:
            try:
                product_id = int(product_id)
                data = request.env['product.template'].search_read(fields=['id','name','categ_id','barcode','list_price','taxes_id','default_code', 'standard_price','qty_available','virtual_available','description_sale','website_published','active','description','inventory_availability','cost_method','categ_id','seller_ids','create_date','weight','lst_price'], domain=[('id', '=', product_id)])
                data_0 = request.env['res.company'].search_read(fields=['street','phone'])
                data_1 = request.env['product.template'].search([('id', '=', data[0]['id'])])
                ret = []
                for line in data_1.attribute_line_ids:
                    for attr in line.value_ids:
                        ret.append({'attribe_id': attr.attribute_id.name, 'value': attr.name})
                _host = request.env['ir.config_parameter'].sudo().search([('key','=','web.base.url')])
                for d in data:
                    d.update({'previewlink' : _host.value+"/shop/product/"+slug(data_1)})
                for d in data:
                    d.update({'image' : _host.value+"/web/content/?model=product.template&field=image_medium&id=%d"%d.get('id',0)})
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data, restrict=['customer_facing_display_html', 'session_ids']),
                    'attribute' : serialization_data(ret),
                    'waranty' : serialization_data(data_0)
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })


    @validate_token
    @http.route(['/api/order/index', '/api/order/index/<name>'], methods=['GET'], type='http', auth='none', csrf=False)
    def getdata_donhang(self, access_token, name=None, **kw):
        if not permission_check('sale.order','read'):
            return generate_response(data={
                'success': False,
                'msg': "Create permission denied"
            })
        if not name:
            try:
                _domain, _fields, _offset, _limit, _order = _args(kw)
                data = request.env['sale.order'].search_read(fields=['id','create_uid','user_id','name','create_date','commitment_date','date_order','confirmation_date','warehouse_id','invoice_status','carrier_id','carrier_code','partner_id'], offset=_offset, limit=_limit, order=_order)
                data2 = request.env['res.partner'].search_read(fields=['id','name','city','state_id','create_date','create_uid','comment','phone','create_uid','commitment_date','cityid'], offset=_offset, limit=_limit, order=_order)
                new_data = []
                for i in range(len(data)):    
                    check = True
                    for k in range(len(data2)):
                        if data[i]['create_uid'] == data2[k]['create_uid']:
                            check = False
                            new_list ={
                                'id': data[i]['id'],
                                'create_uid': data[i]['create_uid'],
                                'user_id': data[i]['user_id'],
                                'name': data[i]['name'],      
                                'create_date': data[i]['create_date'],
                                'date_order': data[i]['date_order'],
                                'confirmation_date': data[i]['confirmation_date'],
                                'commitment_date': data[i]['commitment_date'],
                                'warehouse_id': data[i]['warehouse_id'],
                                'invoice_status': data[i]['invoice_status'],
                                'carrier_id': data[i]['carrier_id'],
                                'partner_id': data[i]['partner_id'],
                                'phone': data2[k]['phone'],
                                'name': data2[k]['name'],
                                'comment' : data2[k]['comment'],
                                'city': data2[k]['city'],
                                'state_id': data2[k]['state_id'],
                                'create_date': data2[k]['create_date'],
                                'create_uid': data[i]['create_uid']
                            }
                            new_data.append(new_list)
                            break
                    if check == True:
                        new_data.append(data[i])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'don hang': serialization_data(new_data),
                    'tong don hang': len(data)
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })
        else:
            try:
                name = str(name)
                data = request.env['sale.order'].search_read(fields=['id','order_id','salesman_id','create_uid','discount'],domain=[('name', '=', name)])
                return generate_response(data={
                    'success': True,
                    'msg': "success",
                    'data': serialization_data(data, restrict=['customer_facing_display_html', 'session_ids'])
                })
            except Exception as e:
                return generate_response(data={
                    'success': False,
                    'msg': "{}".format(e)
                })



